package com.igorz.sectors.validation;

import java.util.List;

public class ValidationResponse {

    private List<String> errors;

    public ValidationResponse(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }
}
