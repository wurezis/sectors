package com.igorz.sectors.repository;


import com.igorz.sectors.model.Sector;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectorRepository extends JpaRepository<Sector, Long> {

}
