package com.igorz.sectors.repository;


import com.igorz.sectors.model.UserSector;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserSectorRepository extends JpaRepository<UserSector, Long> {

    List<UserSector> findAllByUserId(Long userId);
}
