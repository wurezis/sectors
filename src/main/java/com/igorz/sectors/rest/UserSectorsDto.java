package com.igorz.sectors.rest;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserSectorsDto {

    private String name;
    private boolean agree;
    private List<Long> sectorIds;
}
