package com.igorz.sectors.model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_sector")
@SequenceGenerator(name="user_sector_seq", sequenceName="user_sector_seq", allocationSize=1)
public class UserSector {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_sector_seq")
    private Long id;

    private Long userId;
    private Long sectorId;
}
