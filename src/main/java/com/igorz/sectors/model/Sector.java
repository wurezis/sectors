package com.igorz.sectors.model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sector")
@SequenceGenerator(name="sector_seq", sequenceName="sector_seq", allocationSize=1)
public class Sector {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sector_seq")
    private Long id;

    @Column(name = "sector_name")
    private String name;
    private Long parent;
}
