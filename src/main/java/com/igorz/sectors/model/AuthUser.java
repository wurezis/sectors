package com.igorz.sectors.model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "auth_user")
@SequenceGenerator(name="auth_user_seq", sequenceName="auth_user_seq", allocationSize=1)
public class AuthUser {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="auth_user_seq")
    private Long id;

    @Column(name = "user_name")
    private String name;

    private String session;
    private Boolean agree;
}
