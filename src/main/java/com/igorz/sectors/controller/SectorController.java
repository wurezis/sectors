package com.igorz.sectors.controller;

import com.igorz.sectors.model.Sector;
import com.igorz.sectors.rest.UserSectorsDto;
import com.igorz.sectors.service.SectorService;
import com.igorz.sectors.service.UserSectorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class SectorController {

    private final SectorService sectorService;
    private final UserSectorService userSectorService;


    @GetMapping("/api/sectors")
    public List<Sector> getSectors() {
        return sectorService.findAll();
    }

    @GetMapping("/api/user-sectors")
    public UserSectorsDto getUserSectors() {
        return userSectorService.getUserSectors();
    }

    @PostMapping("/api/user-sectors")
    public void addUserSectors(@RequestBody UserSectorsDto userSectorsDto) {
        userSectorService.save(userSectorsDto);
    }
}




