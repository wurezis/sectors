package com.igorz.sectors.handler;

import com.igorz.sectors.validation.ValidationException;
import com.igorz.sectors.validation.ValidationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@ControllerAdvice
public class ValidationExceptionHandler {

    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    public ResponseEntity<ValidationResponse> handleException(ValidationException ex) {
        ex.getErrors().forEach(log::error);
        return new ResponseEntity<>(new ValidationResponse(ex.getErrors()), HttpStatus.PRECONDITION_FAILED);
    }
}
