package com.igorz.sectors.service;

import com.igorz.sectors.model.AuthUser;
import com.igorz.sectors.model.UserSector;
import com.igorz.sectors.repository.UserSectorRepository;
import com.igorz.sectors.rest.UserSectorsDto;
import com.igorz.sectors.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserSectorService {
    private final UserService userService;
    private final UserSectorRepository userSectorRepository;

    public UserSectorsDto getUserSectors() {
        AuthUser user = userService.findBySession();
        if (user != null) {
            List<UserSector> userSectorList = userSectorRepository.findAllByUserId(user.getId());
            return convert(user, userSectorList);
        }
        return new UserSectorsDto();
    }

    private UserSectorsDto convert(AuthUser user, List<UserSector> userSectorList) {
        return UserSectorsDto.builder()
                .name(user.getName())
                .agree(BooleanUtils.isTrue(user.getAgree()))
                .sectorIds(userSectorList.stream().map(UserSector::getSectorId).collect(Collectors.toList()))
                .build();
    }

    public void save(UserSectorsDto userSectorsDto) throws ValidationException {
        validate(userSectorsDto);
        AuthUser user = userService.addOrUpdateUser(userSectorsDto);
        List<Long> newSectorIds = userSectorsDto.getSectorIds();
        List<UserSector> existingSectors = userSectorRepository.findAllByUserId(user.getId());
        List<Long> existingSectorIds = existingSectors.stream().map(UserSector::getSectorId).collect(Collectors.toList());
        addNewSectors(user.getId(), newSectorIds, existingSectorIds);
        deleteSectors(newSectorIds, existingSectors, existingSectorIds);
    }

    private void validate(UserSectorsDto userSectorsDto) throws ValidationException {
        List<String> errors = new ArrayList<>();
        int nameMaxLength = 200;
        if (StringUtils.isBlank(userSectorsDto.getName())) {
            errors.add("errors.name.required");
        } else if (userSectorsDto.getName().length() > nameMaxLength) {
            errors.add("errors.name.maxlength");
        }
        if (CollectionUtils.isEmpty(userSectorsDto.getSectorIds())) {
            errors.add("errors.sector.required");
        }
        if (!userSectorsDto.isAgree()) {
            errors.add("errors.agree.required");
        }
        if (CollectionUtils.isNotEmpty(errors)) {
            throw new ValidationException(errors);
        }
    }

    private void addNewSectors(long userId, List<Long> newSectorIds, List<Long> existingSectorIds) {
        List<Long> sectorIdsToSave = new ArrayList<>();
        newSectorIds.forEach(newSectorId -> {
            if (!existingSectorIds.contains(newSectorId)) {
                sectorIdsToSave.add(newSectorId);
            }
        });
        if (CollectionUtils.isNotEmpty(sectorIdsToSave)) {
            addUserSectors(userId, sectorIdsToSave);
        }
    }

    private void deleteSectors(List<Long> newSectorIds, List<UserSector> existingSectors, List<Long> existingSectorIds) {
        List<UserSector> sectorsToDelete = new ArrayList<>();
        existingSectorIds.forEach(existingSectorId -> {
            if (!newSectorIds.contains(existingSectorId)) {
                sectorsToDelete.addAll(existingSectors.stream()
                        .filter(s -> Objects.equals(s.getSectorId(), existingSectorId)).collect(Collectors.toList()));
            }
        });
        if (CollectionUtils.isNotEmpty(sectorsToDelete)) {
            userSectorRepository.deleteInBatch(sectorsToDelete);
        }
    }

    private void addUserSectors(long userId, List<Long> sectorIds) {
        List<UserSector> userSectorList = new ArrayList<>();
        sectorIds.forEach(sectorId -> userSectorList.add(
                UserSector.builder()
                        .sectorId(sectorId)
                        .userId(userId)
                        .build()));
        userSectorRepository.saveAll(userSectorList);
    }
}
