package com.igorz.sectors.service;

import com.igorz.sectors.model.Sector;
import com.igorz.sectors.repository.SectorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SectorService {

    private final SectorRepository sectorRepository;

    public List<Sector> findAll() {
        return sectorRepository.findAll();
    }
}
