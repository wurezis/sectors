package com.igorz.sectors.service;

import com.igorz.sectors.model.AuthUser;
import com.igorz.sectors.repository.UserRepository;
import com.igorz.sectors.rest.UserSectorsDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;

    public AuthUser addOrUpdateUser(UserSectorsDto userSectorsDto) {
        AuthUser authUser = findBySession();
        return authUser == null ? saveNewUser(userSectorsDto) : updateUser(userSectorsDto, authUser);
    }

    private AuthUser saveNewUser(UserSectorsDto userSectorsDto) {
        return userRepository.save(AuthUser.builder()
                .name(userSectorsDto.getName())
                .session(getSessionId())
                .agree(userSectorsDto.isAgree())
                .build());
    }

    private AuthUser updateUser(UserSectorsDto userSectorsDto, AuthUser user) {
        user.setAgree(userSectorsDto.isAgree());
        user.setName(userSectorsDto.getName());
        userRepository.save(user);
        return user;
    }

    public AuthUser findBySession() {
        String sessionId = getSessionId();
        log.info("Finding user with session " + sessionId);
        return userRepository.findBySession(sessionId).orElse(null);
    }

    private static String getSessionId() {
        return RequestContextHolder.currentRequestAttributes().getSessionId();
    }
}
