package com.igorz.sectors;

import static org.assertj.core.api.Assertions.assertThat;

import com.igorz.sectors.controller.SectorController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SectorsApplicationTests {

    @Autowired
    private SectorController controller;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();
    }
}
