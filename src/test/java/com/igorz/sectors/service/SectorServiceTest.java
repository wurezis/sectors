package com.igorz.sectors.service;

import static org.mockito.Mockito.when;

import com.igorz.sectors.model.Sector;
import com.igorz.sectors.repository.SectorRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
public class SectorServiceTest {

    private SectorService sectorService;

    @MockBean
    private SectorRepository sectorRepository;

    @Before
    public void setUp() {
        sectorService = new SectorService(sectorRepository);
    }

    @Test
    public void whenFindAll_thenReturnSectors() {
        when(sectorRepository.findAll()).thenReturn(getTestSectors());
        List<Sector> result = sectorService.findAll();
        Assert.assertEquals(getTestSectors(), result);
    }

    private static List<Sector> getTestSectors(){
        Sector sector1 = new Sector(1L, "Sector 1", null);
        Sector sector2 = new Sector(2L, "Sector 1", 1L);
        return List.of(sector1, sector2);
    }
}
