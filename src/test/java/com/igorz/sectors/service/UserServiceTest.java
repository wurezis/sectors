package com.igorz.sectors.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.igorz.sectors.model.AuthUser;
import com.igorz.sectors.repository.UserRepository;
import com.igorz.sectors.rest.UserSectorsDto;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Before
    public void setUp() {
        userService = new UserService(userRepository);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(userRepository.save(any())).thenReturn(getNewAuthUser());
    }

    @Test
    public void whenAddUser_thenInsertNew() {
        when(userRepository.findBySession(anyString())).thenReturn(Optional.empty());
        AuthUser result = userService.addOrUpdateUser(getUserSectorsDto());
        Assert.assertEquals(getNewAuthUser(), result);
    }

    @Test
    public void whenUpdateUser_thenUpdateExisting() {
        when(userRepository.findBySession(anyString())).thenReturn(Optional.of(getAuthUser()));
        AuthUser result = userService.addOrUpdateUser(getUserSectorsDto());
        Assert.assertEquals(getAuthUser(), result);
    }


    private static AuthUser getAuthUser(){
        return AuthUser.builder().id(1L).name("Test").session("123").agree(true).build();
    }

    private static AuthUser getNewAuthUser(){
        return AuthUser.builder().id(1L).name("New").session("123").agree(true).build();
    }

    private static UserSectorsDto getUserSectorsDto(){
        return UserSectorsDto.builder().name("Test").agree(true).sectorIds(List.of(1L, 2L)).build();
    }
}
