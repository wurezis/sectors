package com.igorz.sectors.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import com.igorz.sectors.model.AuthUser;
import com.igorz.sectors.model.UserSector;
import com.igorz.sectors.repository.UserSectorRepository;
import com.igorz.sectors.rest.UserSectorsDto;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class UserSectorServiceTest {
    private UserSectorService userSectorService;

    @MockBean
    private UserSectorRepository userSectorRepository;
    @MockBean
    private UserService userService;

    @Before
    public void setUp() {
        userSectorService = new UserSectorService(userService, userSectorRepository);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(userService.findBySession()).thenReturn(getAuthUser());
        when(userService.addOrUpdateUser(any())).thenReturn(getAuthUser());
        when(userSectorRepository.findAllByUserId(anyLong())).thenReturn(getTestUserSectors());
    }

    @Test
    public void whenUserExist_thenReturnUserSectors() {
        UserSectorsDto result = userSectorService.getUserSectors();
        Assert.assertEquals(getUserSectorsDto(), result);
    }

    @Test
    public void whenUserNotExist_thenReturnEmptyUserSectors() {
        when(userService.findBySession()).thenReturn(null);
        UserSectorsDto result = userSectorService.getUserSectors();
        Assert.assertNotNull(result);
        Assert.assertNull(result.getName());
        Assert.assertNull(result.getSectorIds());
    }

    @Test
    public void whenSaveNewSector_thenInsertSectors() {
        UserSectorsDto userSectorsDto = getUserSectorsDto();
        List<Long> sectorIds = new ArrayList<>(userSectorsDto.getSectorIds());
        sectorIds.add(5L);
        userSectorsDto.setSectorIds(sectorIds);
        userSectorService.save(userSectorsDto);
        verify(userSectorRepository, times(1)).saveAll(any());
        verify(userSectorRepository, times(0)).deleteInBatch(any());
    }

    @Test
    public void whenSaveNewSectorAndDelete_thenInsertAndDeleteSectors() {
        UserSectorsDto userSectorsDto = getUserSectorsDto();
        userSectorsDto.setSectorIds(List.of(1L, 5L));
        userSectorService.save(userSectorsDto);
        verify(userSectorRepository, times(1)).saveAll(any());
        verify(userSectorRepository, times(1)).deleteInBatch(any());
    }

    @Test
    public void whenSectorsNotChanged_thenDoNothing() {
        userSectorService.save(getUserSectorsDto());
        verify(userSectorRepository, times(0)).saveAll(any());
        verify(userSectorRepository, times(0)).deleteInBatch(any());
    }

    private static List<UserSector> getTestUserSectors() {
        return List.of(UserSector.builder()
                        .id(1L)
                        .userId(1L)
                        .sectorId(1L)
                        .build(),
                UserSector.builder()
                        .id(2L)
                        .userId(1L)
                        .sectorId(3L)
                        .build());
    }

    private static AuthUser getAuthUser(){
        return AuthUser.builder().id(1L).name("Test").session("123").agree(true).build();
    }

    private static AuthUser getNewAuthUser(){
        return AuthUser.builder().id(1L).name("New").session("123").agree(true).build();
    }

    private static UserSectorsDto getUserSectorsDto(){
        return UserSectorsDto.builder().name("Test").agree(true).sectorIds(List.of(1L, 3L)).build();
    }
}
