package com.igorz.sectors.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.igorz.sectors.model.Sector;
import com.igorz.sectors.rest.UserSectorsDto;
import com.igorz.sectors.service.SectorService;
import com.igorz.sectors.service.UserSectorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

@WebMvcTest(SectorController.class)
public class SectorControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SectorService sectorService;

    @MockBean
    private UserSectorService userSectorService;

    @Test
    public void getSectors() throws Exception {
        when(sectorService.findAll()).thenReturn(getTestSectors());
        this.mockMvc.perform(get("/api/sectors"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(asJsonString(getTestSectors())));
    }

    @Test
    public void getUserSectors() throws Exception {
        UserSectorsDto userSectorsDto = getUserSectorsDto();
        when(userSectorService.getUserSectors()).thenReturn(userSectorsDto);
        this.mockMvc.perform(get("/api/user-sectors"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(asJsonString(userSectorsDto)));
    }

    @Test
    public void addUserSectors() throws Exception {
        when(sectorService.findAll()).thenReturn(getTestSectors());
        UserSectorsDto userSectorsDto = getUserSectorsDto();
        this.mockMvc.perform(post("/api/user-sectors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userSectorsDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }


    private static List<Sector> getTestSectors(){
        Sector sector1 = new Sector(1L, "Sector 1", null);
        Sector sector2 = new Sector(2L, "Sector 1", 1L);
        return List.of(sector1, sector2);
    }

    private static UserSectorsDto getUserSectorsDto() {
        return UserSectorsDto.builder().sectorIds(List.of(1L, 2L)).agree(true).name("Test").build();
    }

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
