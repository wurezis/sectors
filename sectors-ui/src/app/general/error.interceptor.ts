import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status || err.status === 0) {
        if (err.status === 403 || err.status === 401) {
          this.router.navigate(['']);
          return throwError('Access denied');
        } else if (err.status === 412 && err.error.errors) {
          return throwError(err.error.errors);
        }
      }
      if (err.error && err.error.params) {
        return throwError(err.error);
      }
      if (err.error) {
        return throwError(err.error.message);
      }
      return throwError(err.statusText);
    }));
  }
}
