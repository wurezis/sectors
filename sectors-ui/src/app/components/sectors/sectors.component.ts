import {Component, OnInit} from '@angular/core';
import {SectorsService} from "../../services/sectors.service";
import {Sector} from "../models/sectors.model";
import {UserSectors} from "../models/user-sectors.model";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-sectors',
  templateUrl: './sectors.component.html',
  styleUrls: ['./sectors.component.css']
})
export class SectorsComponent implements OnInit {
  sectorChoices: Sector[] = [];
  userSectors: UserSectors = new UserSectors('', false, []);
  submitted = false;
  saved = false;
  errors = [];

  constructor(private sectorsService: SectorsService) {
  }

  ngOnInit(): void {
    this.errors = [];
    this.sectorsService.getSectorChoices().subscribe(sectorChoices => {
      this.sectorChoices = sectorChoices;
      this.sectorsService.getUserSectors().subscribe((userSectors => {
        if (userSectors !== null) {
          this.userSectors = userSectors;
        }
      }));
    });
  }

  save(): void {
    this.errors = [];
    this.submitted = false;
    this.sectorsService.save(this.userSectors).subscribe(() => {
      this.saved = true;
    }, err => {
      this.errors = err;
    });
  }

  onSubmit(form: NgForm) {
    this.saved = false;
    this.submitted = true;
    if (form.valid) {
      this.save()
    }
  }

  onChange() {
    this.submitted = false;
    this.saved = false;
    this.errors = [];
  }

  hasErrors() {
    return this.errors && this.errors.length > 0;
  }
}
