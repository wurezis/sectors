export class Sector {
  constructor(
    public id: number,
    public name: String,
    public parent: number) {
  }
}
