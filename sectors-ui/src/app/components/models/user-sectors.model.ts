export class UserSectors {
  constructor(
    public name: String,
    public agree: boolean,
    public sectorIds: number[]) {
  }
}
