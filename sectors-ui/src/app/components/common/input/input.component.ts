import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'sectors-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ]
})

export class InputComponent implements ControlValueAccessor {
  @Input() label: string = '';
  @Input() name: string = '';
  @Input() id: string = '';
  @Input() disabled = false;
  @Input() showErrors = false;
  @Input() required = false;
  @Input() maxlength: number = 1000;
  @Input() minlength: number = 0;

  control: FormControl = new FormControl();
  value: any = null;

  onChange = (val: any) => {
  };

  onTouched = () => {
  };

  onInputChange(val: any) {
    this.onChange(val);
  }

  onInputTouched() {
    if (this.value) {
      let val = this.value;
      if (val !== this.value) {
        this.value = val;
        this.onInputChange(val);
      }
    }
    this.onTouched();
  }

  writeValue(v: any) {
    this.value = v;
  }

  isValid(): boolean {
    return this.disabled || !this.control || !this.control.dirty || !this.control.touched || this.control.valid;
  }

  registerOnChange(fn: (val: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  validate(c: FormControl) {
    this.control = c;
    return null;
  }
}
