import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';

@Component({
  selector: 'sectors-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    }
  ]
})

export class CheckboxComponent implements ControlValueAccessor {
  @Input() label: string = '';
  @Input() name: string = '';
  @Input() id: string = '';
  @Input() disabled = false;
  @Input() showErrors = false;
  @Input() required = false;

  control: FormControl = new FormControl();
  value: any = null;

  onChange = (val: boolean) => {
  };

  onTouched = () => {
  };

  onInputChange(val: any) {
    this.onChange(val);
  }

  onInputTouched() {
    if (this.value) {
      let val = this.value;
      if (val !== this.value) {
        this.value = val;
        this.onInputChange(val);
      }
    }
    this.onTouched();
  }

  writeValue(v: boolean) {
    this.value = v;
  }

  isValid(): boolean {
    return this.disabled || !this.control || !this.control.dirty || !this.control.touched || this.control.valid;
  }

  registerOnChange(fn: (val: boolean) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  validate(c: FormControl) {
    if (this.required) {
      c.setValidators(Validators.requiredTrue);
    }
    this.control = c;
    return null;
  }
}
