import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';
import {Sector} from "../../models/sectors.model";

@Component({
  selector: 'sectors-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiSelectComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MultiSelectComponent),
      multi: true
    }
  ]
})

export class MultiSelectComponent implements ControlValueAccessor {
  @Input() label: string = '';
  @Input() name: string = '';
  @Input() id: string = '';
  @Input() disabled = false;
  @Input() showErrors = false;
  @Input() required = false;
  @Input() choices: Sector[] = [];

  control: FormControl = new FormControl();
  value: number[] = [];

  onChange = (val: boolean) => {
  };

  onTouched = () => {
  };

  onInputChange(val: any) {
    this.onChange(val);
  }

  onInputTouched() {
    if (this.value) {
      let val = this.value;
      if (val !== this.value) {
        this.value = val;
        this.onInputChange(val);
      }
    }
    this.onTouched();
  }

  writeValue(v: number[]) {
    this.value = v;
  }

  isValid(): boolean {
    return this.disabled || !this.control || !this.control.dirty || !this.control.touched || this.control.valid;
  }

  registerOnChange(fn: (val: boolean) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  validate(c: FormControl) {
    this.control = c;
    return null;
  }

  getParentChoices() {
    return this.choices.filter(s => s.parent === null);
  }

  getChildChoices(id: number) {
    return this.choices.filter(s => s.parent === id);
  }

  isValueSelected(id: number) {
    return this.value.indexOf(id) > -1;
  }
}
