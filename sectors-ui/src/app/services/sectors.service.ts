import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserSectors} from "../components/models/user-sectors.model";
import {Sector} from "../components/models/sectors.model";


@Injectable({
  providedIn: 'root'
})
export class SectorsService {

  constructor(private http: HttpClient) {
  }


  public getSectorChoices(): Observable<Sector[]> {
    return this.http.get<Sector[]>('/api/sectors');
  }

  public getUserSectors(): Observable<UserSectors> {
    return this.http.get<UserSectors>('/api/user-sectors');
  }

  public save(data: UserSectors) {
    return this.http.post<UserSectors>('/api/user-sectors', data);
  }
}
