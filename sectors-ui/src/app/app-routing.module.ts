import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SectorsComponent} from './components/sectors/sectors.component';

const routes: Routes = [
  {path: '', redirectTo: 'sectors', pathMatch: 'full'},
  {path: 'sectors', component: SectorsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
