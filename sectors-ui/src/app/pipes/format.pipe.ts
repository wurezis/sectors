import {Pipe} from '@angular/core';

@Pipe({name: 'format'})
export class FormatPipe {
  transform(value: string, args: any) {
    return FormatPipe.formatString(String(value), args);
  }

  static formatString(str: string, args: any): string {
    let result = str;
    if (!!args && ('object' === typeof args)) {
      Object.keys(args).forEach(key => {
        result = result.replace(new RegExp('\\{' + key + '\\}', 'g'), args[key]);
      });
    }
    return result;
  }
}
