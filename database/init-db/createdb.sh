#!/bin/bash

psql -d "$POSTGRES_DB" -U "$POSTGRES_USER" -w -f /docker-entrypoint-initdb.d/create_user.psql
psql -d "$POSTGRES_DB" -U "$POSTGRES_USER" -w -f /docker-entrypoint-initdb.d/create_database.psql
psql -d "sectors_db" -U "$POSTGRES_USER" -w -f /docker-entrypoint-initdb.d/init_database.psql
psql -d "sectors_db" -U "$POSTGRES_USER" -w -f /docker-entrypoint-initdb.d/create_tables.pslq
psql -d "sectors_db" -U "$POSTGRES_USER" -w -f /docker-entrypoint-initdb.d/insert_data.pslq
