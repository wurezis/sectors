#!/usr/bin/env bash

docker rm -vf $name || true
docker run -d --name=sectorspg \
  -e TZ='Europe/Tallinn' \
  -e POSTGRES_PASSWORD=postgres \
  -p 5999:5432 \
  -d "sectors/postgresql"
