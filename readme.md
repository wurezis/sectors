# Sectors project

## Technical info

| Name | Description |
| ------------- | ------------- |
| Java  | 11.0.10  |
| Gradle  | Gradle 6.8.3, https://gradle.org ("Gradle Wrapper" in use)  |
| Angular  | 11.2.6  |
| Database  | PostgreSQL, from 12.1  |

## Database
### Initialize database
Preferred option is to run PostgreSQL database as Docker container.

Go to database module folder (from project root)
```
cd database/init-db
```

First, build Docker image:
```
./docker-build.sh (Mac, Linux)
docker-build (Windows)
```
Then init and run the container:
```
./docker-run.sh (Mac, Linux)
docker-run (Windows)
```

Database will run on localhost:5999:
```
host: localhost
port: 5999
user: sectors_owner
password: test
database: sectors_db
schema: sectors

jdbc:postgresql://localhost:5999/sectors_db
NB! The application will use user "sectors_app" with restricted privileges
```

## Backend
Java version 11.0.10

### Build and run
As this is typical Spring Boot application then one can run it either from favourite IDE or command line:
```
gradlew bootRun
```
or
```
gradlew bootWar
java -jar ./build/libs/sectors-1.0.war
```
The back-end application will run on http://localhost:9190


### Front-end
**Make sure you have at least nodejs version 10.13 installed**
Go to front-end module folder (from project root)
```
cd sectors-ui
```
To build front end in folder ./sectors-ui use command
```
npm install
```
To run front end application under ./sectors-ui folder use command
```
npm start
```
The front-end application will run on http://localhost:9191/sectors.  
**NB! In case of "ng serve" the app will not use predefined proxy settings.**

